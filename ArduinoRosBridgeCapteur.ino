#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <ArduinoJson.h>
#include <Wire.h>
#include "MS5837.h"
#include "TSYS01.h"
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>

// Ethernet
byte MacAddress[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress MyIPAddress(192, 168, 86, 201);
unsigned int LocalPort = 8888;
IPAddress DestinationAddress(192, 168, 86, 86);
unsigned int DestinationPort = 9090;
EthernetUDP Udp;
char PacketBuffer[UDP_TX_PACKET_MAX_SIZE];
unsigned long LastSend = 0;
const long SendPeriod = 1000;

double tempEau;
double profondeur;
double qW;
double qX;
double qY;
double qZ;

//Initialisation des noms des capteurs
MS5837 pression;
TSYS01 temperature;

//Sample rate (100ms)
#define BNO055_SAMPLERATE_DELAY_MS (100)
Adafruit_BNO055 bno = Adafruit_BNO055();

void setup() {

  // Serial communication
  Serial.begin(9600);
  while (!Serial) continue;

  Wire.begin();

  // Initialisation pressure sensor
  pression.init()
    //Serial.println("Are SDA/SCL connected correctly?");
    //Serial.println("Blue Robotics Bar30: White=SDA, Green=SCL");
    
  //Initialisation temperature sensor
  temperature.init();

  //Definition du modele de capteur pression et de la densite volumique
  pression.setModel(MS5837::MS5837_30BA);
  pression.setFluidDensity(997); // kg/m^3 (freshwater, 1029 for seawater)

  //Initialisation accelerometre
  bno.begin();
  bno.setExtCrystalUse(true);
  
  // Ethernet
  Ethernet.begin(MacAddress, MyIPAddress);
  Udp.begin(LocalPort);
  Serial.println(Ethernet.localIP());

  // Advertise Temperature
  DynamicJsonBuffer  jsonBuffer_Temp(200);
  JsonObject& root_Temp = jsonBuffer_Temp.createObject();
  root_Temp["op"] = "advertise";
  root_Temp["topic"] = "/sensor_Temp";
  root_Temp["type"] = "std_msgs/Float64";
  root_Temp.printTo(Serial);
  Serial.println();
  char jsonChar_Temp[100];
  root_Temp.printTo((char*)jsonChar_Temp, root_Temp.measureLength() + 1);

  // Advertise Profondeur
  DynamicJsonBuffer  jsonBuffer_Depth(200);
  JsonObject& root_Depth = jsonBuffer_Depth.createObject();
  root_Depth["op"] = "advertise";
  root_Depth["topic"] = "/sensor_Depth";
  root_Depth["type"] = "std_msgs/Float64";
  root_Depth.printTo(Serial);
  Serial.println();
  char jsonChar_Depth[100];
  root_Depth.printTo((char*)jsonChar_Depth, root_Depth.measureLength() + 1);

  // Advertise Accelerometre
  DynamicJsonBuffer  jsonBuffer_Acc(200);
  JsonObject& root_Acc = jsonBuffer_Acc.createObject();
  root_Acc["op"] = "advertise";
  root_Acc["topic"] = "/sensor_Acc";
  root_Acc["type"] = "sensor_msgs/Imu";
  root_Acc.printTo(Serial);
  Serial.println();
  char jsonChar_Acc[100];
  root_Acc.printTo((char*)jsonChar_Acc, root_Acc.measureLength() + 1);

  // Ethernet
  Udp.beginPacket(DestinationAddress, DestinationPort);
  Udp.print(jsonChar);
  Udp.endPacket();
}

void loop() {

  // Update pressure and temperature readings
  pression.read();
  temperature.read();
  tempEau = temperature.temperature();
  profondeur = pression.depth();

  //Angle orientation
  imu::Quaternion quat = bno.getQuat();
  qW = quat.w();
  qX = quat.x();
  qY = quat.y();
  qZ = quat.z();

  // Publish Temperature
  DynamicJsonBuffer  jsonBuffer_Temp(200);
  JsonObject& root_Temp = jsonBuffer_Temp.createObject();
  root_Temp["op"] = "publish";
  root_Temp["topic"] = "/sensors_Temp";
  JsonObject& data_Temp = jsonBuffer_Temp.createObject();
  data_Temp["data"] = tempEau;
  root_Temp["msg"] = data_Temp;
  root_Temp.printTo(Serial);
  Serial.println();
  // Convert Json string to char array
  char jsonChar_Temp[100];
  root_Temp.printTo((char*)jsonChar_Temp, root_Temp.measureLength() + 1);

  // Publication Ethernet Temperature
  Udp.beginPacket(DestinationAddress, DestinationPort);
  Udp.print(jsonChar_Temp);
  Udp.endPacket();

  // Publish Profondeur
  DynamicJsonBuffer  jsonBuffer_Depth(200);
  JsonObject& root_Depth = jsonBuffer_Depth.createObject();
  root_Depth["op"] = "publish";
  root_Depth["topic"] = "/sensors_Depth";
  JsonObject& data_Depth = jsonBuffer_Depth.createObject();
  data_Depth["data"] = profondeur;
  root_Depth["msg"] = data_Depth;
  root_Depth.printTo(Serial);
  Serial.println();
  // Convert Json string to char array
  char jsonChar_Depth[100];
  root_Depth.printTo((char*)jsonChar_Depth, root_Depth.measureLength() + 1);

  // Publication Ethernet profondeur
  Udp.beginPacket(DestinationAddress, DestinationPort);
  Udp.print(jsonChar_Depth);
  Udp.endPacket();

  // Publish Accelerometre
  DynamicJsonBuffer  jsonBuffer_Acc(200);
  JsonObject& root_Acc = jsonBuffer_Acc.createObject();
  root_Acc["op"] = "publish";
  root_Acc["topic"] = "/sensors_Acc";
  JsonObject& data_Acc = jsonBuffer_Acc.createObject();
  data_Acc["x"] = qX;
  data_Acc["y"] = qY;
  data_Acc["z"] = qZ;
  data_Acc["w"] = qW;
  root_Acc["orientation"] = data_Acc;
  root_Acc.printTo(Serial);
  Serial.println();
  // Convert Json string to char array
  char jsonChar_Acc[100];
  root_Acc.printTo((char*)jsonChar_Acc, root_Acc.measureLength() + 1);

  // Publication Ethernet Accelerometre
  Udp.beginPacket(DestinationAddress, DestinationPort);
  Udp.print(jsonChar_Acc);
  Udp.endPacket();

  delay(100);
}

