# README #

### SUMMARY ###

* Arduino node for aquiring sensor information (temperature, depth and accelerometer)
* Data serialisation in JSON format
* Sends serialised data via UDP ethernet to ROS master node
* Each sensor has its own ROS topic (one for temperature, one for depth(pressure) and one for accelerometer data)

### SETUP ###

* Must download Arduino libraries:
*	-https://github.com/bluerobotics/BlueRobotics_TSYS01_Library
*	-https://github.com/bluerobotics/BlueRobotics_MS5837_Library
*	-https://github.com/adafruit/Adafruit_BNO055
*	-https://github.com/bblanchon/ArduinoJson
*	-https://github.com/adafruit/Adafruit_Sensor